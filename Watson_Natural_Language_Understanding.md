<img src="images/IDSNlogo.png" width = "300">


# Hands on Lab: Creating Natural Language Understanding service
**Estimated Effort:** 20 minutes

## Lab overview:

**Watson Natural Language Understanding** offers a suite of features for text analysis.

The Watson Natural Language Understanding analyze text to extract metadata from content such as concepts, entities, keywords, categories, sentiment, emotion, relations, and semantic roles using natural language understanding.

## Objectives:
After completing this lab, you will be able to:
* Create an IBM Cloud Lite Account 
* Create a Natural Language Understanding service

## Exercise 1: Create an IBM Cloud Account

#### Scenario

To access the resources and services that the IBM Cloud provides, you need an IBM Cloud account. 

Note: If you already have an IBM Cloud account, Login by going to https://cloud.ibm.com/login and skip ahead to Exercise 2.

#### Task 1: Sign up for IBM Cloud

1.  Go to: [https://cloud.ibm.com/registration](https://cloud.ibm.com/registration) to create a free account on IBM Cloud.

Note: If you already have an IBM Cloud account, Login by going to [https://cloud.ibm.com/login](https://cloud.ibm.com/login) and skip ahead to Exercise 2.

2. Enter your **Email** address and a strong **Password** and then click the **Next** button.

![](./images/1.jpg)

3. An email is sent to the address that you signed up with to confirm your email address. Check your email and copy and paste **Verification code**. Then click **Next**.

![](./images/2.jpg)

4. Once your email is successfully verified, enter your **First name**, **Last name**, and **Country** and click **Next**.

![](./images/3.jpg)

5. After your personal information is accepted Click on **Create account** button.

![](./images/4.jpg)
 It takes a few seconds to create and set up your account.

6. You will see this page which confirms your account creation and allows you to login.
![](./images/5.png)

7. The username (which is your email address) is already populated. Enter your password and login.
![](./images/6.png)

8. Read carefully about the IBMid Privacy and click on proceed to acknowledge and login.
![](./images/7.png)

9. Once you successfully login, you should see the dashboard.Now, please click on **Create resource** and proceed to the next exercise.
![](./images/8.png)

# Exercise 2: Create an instance of Natural Language Understanding service

## Scenario

Natural Language Understanding, which is available as a service on IBM Cloud. In this exercise, you will add an instance of the Natural Language Understanding service to your IBM Cloud account.

## Task 1: Add Natural Language Understanding as a resource

1. On the [Catalog](https://cloud.ibm.com/catalog) page, click on the **Services** , select the **AI/ Machine learning** category and then select the **Natural Language Understanding** resource.

![](./images/9.png)

2. On the Natural Language Understanding page, select **DALLAS** as the Region, verify that the **Lite** plan is selected, and then click **Create**.

![](./images/10.png)

3. Once the Natural Language Understanding instance is successfully created, Please click on the **Manage**, you'll get the below screen where you can copy the **API** key by clicking on the copy icon.:

![](./images/11.png)

## Author(s)

 [Shubham Yadav](https://www.linkedin.com/in/shubham-kumar-yadav-14378768)

## Contributor(s)

 [Lavanya T S](https://www.linkedin.com/in/lavanya-sunderarajan-199a445)

 ## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 01-12-2020 | 2.0 | Shubham | Created Lab instructions |
|   |   |   |   |
